package tema1.demo.entities;


import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="medicationPlan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idMedicationPlan", unique = true, nullable = false)
    private  int idMedicationPlan;

    @ManyToMany(mappedBy="medicationPlanList")
    Set<Medication> medicationList = new HashSet<Medication>();


    @ManyToMany(mappedBy="patientMedList")
    Set<Patient> patientList = new HashSet<Patient>();

    @Column(name = "intakeInterval", length = 100)
    private String intakeInterval;

    @Column(name = "period", length = 100)
    private String period;

    public MedicationPlan(String intakeInterval,String period, Set<Medication> medicationList,  Set<Patient> patientList){
       this.setIntakeInterval(intakeInterval);
       this.setPeriod(period);
       this.setMedicationList(medicationList);
       this.setPatientList(patientList);

    }

    public MedicationPlan(){}

    public Set<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(Set<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public Set<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(Set<Patient> patientList) {
        this.patientList = patientList;
    }

    public String getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(String intakeInterval) {
        this.intakeInterval = intakeInterval;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "MedicationPlan{" +
                "medicationList=" + medicationList +
                ", patientList=" + patientList +
                ", intakeInterval='" + intakeInterval + '\'' +
                ", period='" + period + '\'' +
                '}';
    }
}
