package tema1.demo.entities;

import org.hibernate.annotations.GeneratorType;
import org.hibernate.boot.model.source.internal.hbm.PluralAttributeSourceSetImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.core.GrantedAuthority;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class User {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int userId;
    private String username;

    @Column(nullable = false)
    private String password;
    private String roleType = "";

    private int active;


    private String permissions = "";

    public User(String username, String password, String roleType, String permissions){
        this.username = username;
        this.password = password;
        this.roleType = roleType;
        this.permissions = permissions;
        this.active = 1;
    }
    public User(){

    }

    public User(int userId, String username,String password, String roleType){
        this.setUserId(userId);
        this.setUsername(username);
        this.setPassword(password);
        this.setRoleType(roleType);
    }



    public User(String username, String name, String password) {

        this.setUsername(username);
        this.setPassword(password);

    }



    public User(String username, String password, List<GrantedAuthority> authorityList) {

        this.setUsername(username);
        this.setPassword(password);

    }



    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }



    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roleType=" + roleType +
                '}';
    }

    public List<String> getRoleList(){
        if(this.roleType.length() > 0){
            return Arrays.asList(this.roleType.split(","));
        }
        return new ArrayList<>();
    }

    public List<String> getPermissionList(){
        if(this.permissions.length() > 0){
            return Arrays.asList(this.permissions.split(","));
        }
        return new ArrayList<>();
    }

    public int getActive() {
        return active;
    }

    public String getPermissions() {
        return permissions;
    }
}
