package tema1.demo.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient extends User implements Serializable {


    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "gender", length = 50)
    private String gender;

    @Column(name= "username", length = 100)
    private String username;

    @Column(name= "password", length = 100)
    private String password;

    @Column(name = "address", length = 100)
    private String address;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "patient_caregiver",
            joinColumns = @JoinColumn(name = "idPatient"),
            inverseJoinColumns = @JoinColumn(name = "idCaregiver"))
            @JsonBackReference
    Set<Caregiver> caregiverList = new HashSet<>();


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "patient_doctor",
            joinColumns = @JoinColumn(name = "idPatient"),
            inverseJoinColumns = @JoinColumn(name = "idDoctor"))
    Set<Doctor> doctorList = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "patient_medicationPlan",
            joinColumns = @JoinColumn(name = "idPatientn"),
            inverseJoinColumns = @JoinColumn(name = "idMedicationPlan"))
    Set<MedicationPlan> patientMedList = new HashSet<>();

    public Patient(){

    }
    public Patient(int userId, String name, Date birthdate, String gender,String username, String password, String address, String roleType){
        this.setUserId(userId);
        this.setName(name);
        this.setBirthdate(birthdate);
        this.setGender(gender);
        this.setAddress(address);
        this.setUsername(username);
        this.setPassword(password);
        this.setRoleType(roleType);
    }

    public Patient(int userId, String name, String username){
        this.setUserId(userId);
        this.setName(name);
        this.setUsername(username);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Caregiver> getCaregiverList() {
        return caregiverList;
    }

    public void setCaregiverList(Set<Caregiver> caregiverList) {
        this.caregiverList = caregiverList;
    }

    public Set<Doctor> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(Set<Doctor> doctorList) {
        this.doctorList = doctorList;
    }

    public Set<MedicationPlan> getPatientMedList() {
        return patientMedList;
    }

    public void setPatientMedList(Set<MedicationPlan> patientMedList) {
        this.patientMedList = patientMedList;
    }

    @Override
    public String toString() {
        return "Patient{" +
                ", name='" + name + '\'' +
                ", birthdate=" + birthdate +
                ", gender='" + gender + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}


