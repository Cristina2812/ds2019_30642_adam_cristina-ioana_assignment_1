package tema1.demo.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.context.annotation.Role;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="caregiver")
public class Caregiver extends User implements Serializable {

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "gender", length = 50)
    private String gender;

    @Column(name= "username", length = 100)
    private String username;

    @Column(name= "password", length = 100)
    private String password;

    @ManyToMany(mappedBy="caregiverList")
            @JsonManagedReference
    Set<Patient> patientList = new HashSet<>();


    @ManyToMany(mappedBy="caregiverList")
    Set<Doctor> doctorList = new HashSet<>();

    public Caregiver(){

    }


    public Caregiver(int userId, String name, Date birthdate,String gender, String username, String password, String roleType){
        this.setUserId(userId);
        this.setName(name);
        this.setBirthdate(birthdate);
        this.setGender(gender);
        this.setUsername(username);
        this.setPassword(password);
        this.setRoleType(roleType);
    }

    public Caregiver(int userId, String name, String username){
        this.setUserId(userId);
        this.setName(name);
        this.setUsername(username);
    }

    public Set<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(Set<Patient> patientList) {
        this.patientList = patientList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Doctor> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(Set<Doctor> doctorList) {
        this.doctorList = doctorList;
    }

    @Override
    public String toString() {
        return "Caregiver{" +
                ", name='" + name + '\'' +
                ", birthdate=" + birthdate +
                ", gender='" + gender + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
