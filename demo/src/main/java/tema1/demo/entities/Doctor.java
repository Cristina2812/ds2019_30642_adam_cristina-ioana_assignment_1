package tema1.demo.entities;



import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "doctor")
public class Doctor extends User implements Serializable {



    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "gender", length = 50)
    private String gender;

    @Column(name= "username", length = 100)
    private String username;

    @Column(name= "password", length = 100)
    private String password;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "doctor_caregiver",
            joinColumns = @JoinColumn(name = "idDoctor"),
            inverseJoinColumns = @JoinColumn(name = "idCaregiver"))
    Set<Caregiver> caregiverList = new HashSet<>();


    @ManyToMany(mappedBy="doctorList")
    Set<Patient> patientList = new HashSet<>();




    public Doctor(){

    }

    public Doctor(int userId,String name, Date birthdate,String gender, String username, String password, String roleType){
        this.setUserId(userId);
        this.setName(name);
        this.setBirthdate(birthdate);
        this.setGender(gender);
        this.setUsername(username);
        this.setPassword(password);
        this.setRoleType(roleType);

    }
    public Doctor(int userId,String name, String username){
        this.setUserId(userId);
        this.setName(name);
        this.setUsername(username);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return "Doctor{" +
                ", name='" + name + '\'' +
                ", birthdate=" + birthdate +
                ", gender='" + gender + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }


}

