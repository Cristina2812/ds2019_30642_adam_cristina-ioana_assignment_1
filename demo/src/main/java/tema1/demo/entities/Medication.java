package tema1.demo.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="medication")
public class Medication {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue
    private int idMedication;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "dosage")
    private String dosage;

    @Column(name = "effect")
    private String effect;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "medication_medicationPlan",
            joinColumns = @JoinColumn(name = "idMedication"),
            inverseJoinColumns = @JoinColumn(name = "idMedicationPlan"))
    Set<MedicationPlan> medicationPlanList;

    public Medication(){

    }

    public Medication(int idMedication,String name, String dosage,  String effect){
        this.setIdMedication(idMedication);
        this.setName(name);
        this.setDosage(dosage);
        this.setEffect(effect);


    }


    public int getIdMedication() {
        return idMedication;
    }

    public void setIdMedication(int idMedication) {
        this.idMedication = idMedication;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "idMedication=" + idMedication +
                ", name='" + name + '\'' +
                ", dosage='" + dosage + '\'' +
                ", effect=" + effect +
                '}';
    }
}
