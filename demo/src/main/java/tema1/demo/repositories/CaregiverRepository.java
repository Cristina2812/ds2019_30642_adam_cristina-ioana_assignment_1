package tema1.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tema1.demo.entities.Caregiver;

import java.util.List;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver,Integer> {

    @Query(value = "SELECT u " +
            "FROM Caregiver u " +
            "ORDER BY u.name")
    List<Caregiver> getAllOrdered();

}
