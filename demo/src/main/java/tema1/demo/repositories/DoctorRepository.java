package tema1.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import tema1.demo.entities.Doctor;


import java.util.List;

@Repository
@PreAuthorize("hasRole('ROLE_MANAGER')")
public interface DoctorRepository extends JpaRepository<Doctor,Integer> {

    @Query(value = "SELECT u " +
            "FROM Doctor u " +
            "ORDER BY u.name")
    List<Doctor> getAllOrdered();


}
