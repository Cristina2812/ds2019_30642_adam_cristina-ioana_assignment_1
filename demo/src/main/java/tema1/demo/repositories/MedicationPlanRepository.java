package tema1.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tema1.demo.entities.MedicationPlan;

import java.util.List;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {

    @Query(value = "SELECT u " +
            "FROM MedicationPlan u " +
            "ORDER BY u.intakeInterval")
    List<MedicationPlan> getAllOrdered();
}

