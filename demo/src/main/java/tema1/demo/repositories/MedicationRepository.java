package tema1.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tema1.demo.entities.Medication;

import java.util.List;


@Repository
public interface MedicationRepository extends JpaRepository<Medication, Integer> {

    @Query(value = "SELECT u " +
            "FROM Medication u " +
            "ORDER BY u.name")
    List<Medication> getAllOrdered();
}
