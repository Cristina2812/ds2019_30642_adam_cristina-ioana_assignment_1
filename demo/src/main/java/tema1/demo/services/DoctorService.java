package tema1.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tema1.demo.dto.DoctorDTO;
import tema1.demo.dto.DoctorViewDTO;
import tema1.demo.dto.builders.DoctorBuilder;
import tema1.demo.dto.builders.DoctorViewBuilder;
import tema1.demo.entities.Doctor;
import tema1.demo.errorhandler.ResourceNotFoundException;
import tema1.demo.repositories.DoctorRepository;
import tema1.demo.validators.DoctorFieldValidator;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorViewDTO findUserById(Integer id){
        Optional<Doctor> doctor  = doctorRepository.findById(id);

        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "user id", id);
        }
        return DoctorViewBuilder.generateDTOFromEntity(doctor.get());
    }

    public List<DoctorViewDTO> findAll(){
        List<Doctor> doctors = doctorRepository.getAllOrdered();

        return doctors.stream()
                .map(DoctorViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(DoctorDTO doctorDTO) {

        DoctorFieldValidator.validateInsertOrUpdate(doctorDTO);

        return doctorRepository
                .save(DoctorBuilder.generateEntityFromDTO(doctorDTO))
                .getUserId();
    }

    public Integer update(DoctorDTO doctorDTO) {

        Optional<Doctor> doctor = doctorRepository.findById(doctorDTO.getIdDoctor());

        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Doctor", "user id", doctorDTO.getIdDoctor());
        }
        DoctorFieldValidator.validateInsertOrUpdate(doctorDTO);

        return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getUserId();
    }

    public void delete(DoctorViewDTO doctorViewDTO){
        this.doctorRepository.deleteById(doctorViewDTO.getId());
    }

}
