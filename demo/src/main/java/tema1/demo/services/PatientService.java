package tema1.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tema1.demo.dto.PatientDTO;
import tema1.demo.dto.PatientViewDTO;
import tema1.demo.dto.builders.PatientBuilder;
import tema1.demo.dto.builders.PatientViewBuilder;
import tema1.demo.entities.Medication;
import tema1.demo.entities.MedicationPlan;
import tema1.demo.entities.Patient;
import tema1.demo.errorhandler.ResourceNotFoundException;
import tema1.demo.repositories.MedicationPlanRepository;
import tema1.demo.repositories.MedicationRepository;
import tema1.demo.repositories.PatientRepository;
import tema1.demo.validators.PatientFieldValidator;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private final PatientRepository patientRepository;
    @Autowired
    private  MedicationRepository medicationRepository;
    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }



    public PatientDTO findUserById(Integer id){
        Optional<Patient> patient  = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return PatientBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientDTO> findAll(){
        List<Patient> patients = patientRepository.getAllOrdered();

        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDTO patientDTO) {

        PatientFieldValidator.validateInsertOrUpdate(patientDTO);

        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getUserId();
    }

    public Integer update(PatientDTO patientDTO) {

        Optional<Patient> patient = patientRepository.findById(patientDTO.getIdPatient());

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "user id", patientDTO.getIdPatient());
        }
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);

        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getUserId();
    }

    public MedicationPlan addMed(Integer patId, Integer medId, MedicationPlan medPlan){

        Optional<Patient> patient  = patientRepository.findById(patId);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", patId);
        }

        Optional<Medication> medication  = medicationRepository.findById(medId);

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "user id", medId);
        }

        MedicationPlan medicationPlan = new MedicationPlan();
        medicationPlan.setIntakeInterval(medPlan.getIntakeInterval());
        medicationPlan.setPeriod(medPlan.getPeriod());

         medicationPlan.getPatientList().add(patient.get());
         medicationPlan.getMedicationList().add(medication.get());

        return medicationPlanRepository.save(medicationPlan);
    }


    public void delete(PatientViewDTO patientViewDTO){
        this.patientRepository.deleteById(patientViewDTO.getId());
    }
}
