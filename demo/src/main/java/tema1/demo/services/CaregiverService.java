package tema1.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.security.krb5.internal.PAEncTSEnc;
import tema1.demo.dto.CaregiverDTO;
import tema1.demo.dto.CaregiverViewDTO;
import tema1.demo.dto.builders.CaregiverBuilder;
import tema1.demo.dto.builders.CaregiverViewBuilder;
import tema1.demo.entities.Caregiver;
import tema1.demo.entities.Medication;
import tema1.demo.entities.MedicationPlan;
import tema1.demo.entities.Patient;
import tema1.demo.errorhandler.ResourceNotFoundException;
import tema1.demo.repositories.CaregiverRepository;
import tema1.demo.repositories.PatientRepository;
import tema1.demo.validators.CaregiverFieldValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;
    private PatientRepository patientRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, PatientRepository patientRepository) {
        this.caregiverRepository = caregiverRepository;
        this.patientRepository = patientRepository;
    }



//    @Autowired
//    public PatientService(PatientRepository patientRepository){this.patientRepository = patientRepository;}

    public CaregiverDTO findUserById(Integer id){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }
        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.getAllOrdered();

        return caregivers.stream()
                .map(CaregiverBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(CaregiverDTO caregiverDTO) {

        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);

        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO))
                .getUserId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {

        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getIdCaregiver());

        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "user id", caregiverDTO.getIdCaregiver());
        }
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getUserId();
    }

    public void delete(CaregiverViewDTO caregiverViewDTO){
        this.caregiverRepository.deleteById(caregiverViewDTO.getId());
    }

    public Set<Patient> getPatients(CaregiverDTO caregiverDTO){
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getIdCaregiver());

        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "user id", caregiverDTO.getIdCaregiver());
        }
        Caregiver c = new Caregiver();
        return c.getPatientList();
    }



    public Caregiver addList(Integer patId, Integer caregiverId){

        Optional<Patient> patient  = patientRepository.findById(patId);

        if (!patient.isPresent()) {
            System.out.println(patient.get());
            throw new ResourceNotFoundException("Patient", "user id", patId);
        }

        Optional<Caregiver> caregiver  = caregiverRepository.findById(caregiverId);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", caregiverId);
        }


        caregiver.get().getPatientList().add(patient.get());
        patient.get().getCaregiverList().add(caregiver.get());

        patientRepository.save(patient.get());

        return caregiverRepository.save(caregiver.get());

    }



}
