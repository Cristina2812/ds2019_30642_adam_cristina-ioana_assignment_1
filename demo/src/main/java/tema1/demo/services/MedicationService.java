package tema1.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tema1.demo.dto.MedicationDTO;
import tema1.demo.dto.builders.MedicationBuilder;
import tema1.demo.entities.Medication;
import tema1.demo.errorhandler.ResourceNotFoundException;
import tema1.demo.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public MedicationDTO findUserById(Integer id){
        Optional<Medication> medication  = medicationRepository.findById(id);

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "user id", id);
        }
        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }

    public List<MedicationDTO> findAll(){
        List<Medication> medications = medicationRepository.getAllOrdered();

        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationDTO medicationDTO) {

        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getIdMedication();
    }

    public Integer update(MedicationDTO medicationDTO) {

        Optional<Medication> medication = medicationRepository.findById(medicationDTO.getIdMedication());

        if(!medication.isPresent()){
            throw new ResourceNotFoundException("Medication", "user id", medicationDTO.getIdMedication());
        }


        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getIdMedication();
    }

    public void delete(MedicationDTO medicationDTO){
        this.medicationRepository.deleteById(medicationDTO.getIdMedication());
    }
}
