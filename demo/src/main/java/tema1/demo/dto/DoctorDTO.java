package tema1.demo.dto;

import java.sql.Date;
import java.util.Objects;

public class DoctorDTO {

    private int idDoctor;
    private String name;
    private Date birthdate;
    private String gender;
    private String username;
    private String password;
    private String roleType;

    public DoctorDTO(){

    }

    public DoctorDTO(int idDoctor,String name, Date birthdate,String gender, String username, String password, String roleType){
        this.idDoctor = idDoctor;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.username = username;
        this.password = password;
        this.roleType = roleType;

    }

    public int getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(int idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!( o instanceof DoctorDTO )) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return getIdDoctor() == doctorDTO.getIdDoctor() &&
                Objects.equals(getName(), doctorDTO.getName()) &&
                Objects.equals(getBirthdate(), doctorDTO.getBirthdate()) &&
                Objects.equals(getGender(), doctorDTO.getGender()) &&
                Objects.equals(getUsername(), doctorDTO.getUsername()) &&
                Objects.equals(getPassword(), doctorDTO.getPassword()) &&
                Objects.equals(roleType, doctorDTO.roleType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdDoctor(), getName(), getBirthdate(), getGender(), getUsername(), getPassword(), roleType);
    }
}
