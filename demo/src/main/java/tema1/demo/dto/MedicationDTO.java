package tema1.demo.dto;


import java.util.Objects;

public class MedicationDTO {

    private int idMedication;
    private String name;
    private String dosage;
    private String effect;


    public MedicationDTO(){

    }

    public MedicationDTO(int idMedication,String name, String dosage, String effect){
        this.idMedication = idMedication;
        this.name = name;
        this.dosage = dosage;
        this.effect = effect;

    }


    public int getIdMedication() {
        return idMedication;
    }

    public void setIdMedication(int idMedication) {
        this.idMedication = idMedication;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!( o instanceof MedicationDTO )) return false;
        MedicationDTO that = (MedicationDTO) o;
        return getIdMedication() == that.getIdMedication() &&
                getName().equals(that.getName()) &&
                getDosage().equals(that.getDosage()) &&
                getEffect().equals(that.getEffect());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdMedication(), getName(), getDosage(), getEffect());
    }
}
