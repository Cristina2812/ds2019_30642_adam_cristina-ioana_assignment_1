package tema1.demo.dto;

import java.sql.Date;
import java.util.Objects;

public class PatientDTO {

    private int idPatient;
    private String name;
    private Date birthdate;
    private String gender;
    private String username;
    private String password;
    private String address;
    private String roleType;

    public PatientDTO(){

    }

    public PatientDTO(int idPatient,String name, Date birthdate,String gender, String username, String password, String address, String roleType){
        this.idPatient = idPatient;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.username = username;
        this.password = password;
        this.address = address;
        this.roleType = roleType;
    }

    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int idPatient) {
        this.idPatient = idPatient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!( o instanceof PatientDTO )) return false;
        PatientDTO that = (PatientDTO) o;
        return getIdPatient() == that.getIdPatient() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getBirthdate(), that.getBirthdate()) &&
                Objects.equals(getGender(), that.getGender()) &&
                Objects.equals(getUsername(), that.getUsername()) &&
                Objects.equals(getPassword(), that.getPassword()) &&
                Objects.equals(getAddress(), that.getAddress()) &&
                Objects.equals(getRoleType(), that.getRoleType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdPatient(), getName(), getBirthdate(), getGender(), getUsername(), getPassword(), getAddress(), getRoleType());
    }
}
