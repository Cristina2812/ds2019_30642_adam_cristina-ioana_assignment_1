package tema1.demo.dto.builders;

import tema1.demo.dto.MedicationDTO;
import tema1.demo.entities.Medication;

public class MedicationBuilder {

    public MedicationBuilder() {

    }

    public static MedicationDTO generateDTOFromEntity(Medication medication) {
        return new MedicationDTO(
                medication.getIdMedication(),
                medication.getName(),
                medication.getDosage(),
                medication.getEffect());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO){

        return new Medication(
                medicationDTO.getIdMedication(),
                medicationDTO.getName(),
                medicationDTO.getDosage(),
                medicationDTO.getEffect());
    }
}
