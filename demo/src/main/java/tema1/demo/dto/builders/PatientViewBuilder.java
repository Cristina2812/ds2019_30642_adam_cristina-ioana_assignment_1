package tema1.demo.dto.builders;

import tema1.demo.dto.PatientViewDTO;
import tema1.demo.entities.Patient;

public class PatientViewBuilder {

    public static PatientViewDTO generateDTOFromEntity(Patient patient){
        return new PatientViewDTO(
                patient.getUserId(),
                patient.getName(),
                patient.getUsername());
    }

    public static Patient generateEntityFromDTO(PatientViewDTO patientViewDTO){
        return new Patient(
                patientViewDTO.getId(),
                patientViewDTO.getName(),
                patientViewDTO.getUsername());
    }
}
