package tema1.demo.dto.builders;

import tema1.demo.dto.DoctorDTO;
import tema1.demo.entities.Doctor;

public class DoctorBuilder {

    public DoctorBuilder() {

    }

    public static DoctorDTO generateDTOFromEntity(Doctor doctor) {
        return new DoctorDTO(
                doctor.getUserId(),
                doctor.getName(),
                doctor.getBirthdate(),
                doctor.getGender(),
                doctor.getUsername(),
                doctor.getPassword(),
                doctor.getRoleType());
    }

    public static  Doctor generateEntityFromDTO(DoctorDTO doctorDTO){

        return new Doctor(
                doctorDTO.getIdDoctor(),
                doctorDTO.getName(),
                doctorDTO.getBirthdate(),
                doctorDTO.getGender(),
                doctorDTO.getUsername(),
                doctorDTO.getPassword(),
                doctorDTO.getRoleType());
    }
}
