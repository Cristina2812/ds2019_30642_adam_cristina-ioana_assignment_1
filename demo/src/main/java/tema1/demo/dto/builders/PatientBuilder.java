package tema1.demo.dto.builders;

import tema1.demo.dto.PatientDTO;
import tema1.demo.entities.Patient;

public class PatientBuilder {

    public PatientBuilder() {

    }

    public static PatientDTO generateDTOFromEntity(Patient patient) {
        return new PatientDTO(
                patient.getUserId(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getAddress(),
                patient.getRoleType());
    }

    public static  Patient generateEntityFromDTO(PatientDTO patientDTO){

        return new Patient(
                patientDTO.getIdPatient(),
                patientDTO.getName(),
                patientDTO.getBirthdate(),
                patientDTO.getGender(),
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getAddress(),
                patientDTO.getRoleType());
    }

}
