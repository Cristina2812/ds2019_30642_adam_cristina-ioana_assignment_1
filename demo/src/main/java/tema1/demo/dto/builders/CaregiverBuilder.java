package tema1.demo.dto.builders;

import tema1.demo.dto.CaregiverDTO;
import tema1.demo.entities.Caregiver;

public class CaregiverBuilder {

    public CaregiverBuilder() {

    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver) {
        return new CaregiverDTO(
                caregiver.getUserId(),
                caregiver.getName(),
                caregiver.getBirthdate(),
                caregiver.getGender(),
                caregiver.getUsername(),
                caregiver.getPassword(),
                caregiver.getRoleType());
    }

    public static  Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO){

        return new Caregiver(
                caregiverDTO.getIdCaregiver(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthdate(),
                caregiverDTO.getGender(),
                caregiverDTO.getUsername(),
                caregiverDTO.getPassword(),
                caregiverDTO.getRoleType());
    }
}
