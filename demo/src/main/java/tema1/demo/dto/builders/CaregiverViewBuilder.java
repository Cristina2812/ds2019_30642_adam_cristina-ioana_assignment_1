package tema1.demo.dto.builders;

import tema1.demo.dto.CaregiverDTO;
import tema1.demo.dto.CaregiverViewDTO;
import tema1.demo.entities.Caregiver;

public class CaregiverViewBuilder {

    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverViewDTO(
                caregiver.getUserId(),
                caregiver.getName(),
                caregiver.getUsername());
    }

    public static Caregiver generateEntityFromDTO(CaregiverViewDTO caregiverViewDTO){
        return new Caregiver(
                caregiverViewDTO.getId(),
                caregiverViewDTO.getName(),
                caregiverViewDTO.getUsername());
    }

}
