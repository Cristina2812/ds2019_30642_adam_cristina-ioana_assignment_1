package tema1.demo.dto.builders;


import tema1.demo.dto.DoctorViewDTO;
import tema1.demo.entities.Doctor;

public class DoctorViewBuilder {

    public static DoctorViewDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorViewDTO(
                doctor.getUserId(),
                doctor.getName(),
                doctor.getUsername());
    }

    public static Doctor generateEntityFromDTO(DoctorViewDTO doctorViewDTO){
        return new Doctor(
                doctorViewDTO.getId(),
                doctorViewDTO.getName(),
                doctorViewDTO.getUsername());
    }

}
