package tema1.demo.dto;

import java.sql.Date;
import java.util.Objects;

public class CaregiverDTO {


    private int idCaregiver;
    private String name;
    private Date birthdate;
    private String gender;
    private String username;
    private String password;
    private String roleType;

    public CaregiverDTO(){

    }

    public CaregiverDTO(int idCaregiver,String name, Date birthdate,String gender, String username, String password, String roleType){
      this.idCaregiver = idCaregiver;
      this.name = name;
      this.birthdate = birthdate;
      this.gender = gender;
      this.username = username;
      this.password = password;
      this.roleType = roleType;

    }

    public int getIdCaregiver() {
        return idCaregiver;
    }

    public void setIdCaregiver(int idCaregiver) {
        this.idCaregiver = idCaregiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!( o instanceof CaregiverDTO )) return false;
        CaregiverDTO that = (CaregiverDTO) o;
        return getIdCaregiver() == that.getIdCaregiver() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getBirthdate(), that.getBirthdate()) &&
                Objects.equals(getGender(), that.getGender()) &&
                Objects.equals(getUsername(), that.getUsername()) &&
                Objects.equals(getPassword(), that.getPassword()) &&
                Objects.equals(getRoleType(), that.getRoleType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdCaregiver(), getName(), getBirthdate(), getGender(), getUsername(), getPassword(), getRoleType());
    }
}
