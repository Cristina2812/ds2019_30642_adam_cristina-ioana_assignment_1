package tema1.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import tema1.demo.dto.PatientDTO;
import tema1.demo.dto.PatientViewDTO;
import tema1.demo.entities.MedicationPlan;
import tema1.demo.services.PatientService;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/{id}")
    public PatientDTO findById(@PathVariable("id") Integer id){
        return patientService.findUserById(id);
    }

    @GetMapping(value = "/")
    public List<PatientDTO> findAll(){
        return patientService.findAll();
    }

    @PostMapping(value = "/")
    public Integer insertUserDTO(@RequestBody PatientDTO patientDTO){

        patientDTO.setRoleType("PATIENT");
        return patientService.insert(patientDTO);
    }

    @PutMapping(value = "/")
    public Integer updateUser(@RequestBody PatientDTO patientDTO) {
        return patientService.update(patientDTO);
    }

    @DeleteMapping(value = "/")
    public void delete(@RequestBody PatientViewDTO patientViewDTO){
        patientService.delete(patientViewDTO);
    }

    @PutMapping(value = "/{patId}/{medId}")
    public MedicationPlan addMedPlan(@PathVariable("patId") Integer patId, @PathVariable("medId") Integer medId, @RequestBody MedicationPlan medicationPlan){
        return patientService.addMed(patId,medId,medicationPlan);

    }


}
