package tema1.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import tema1.demo.dto.CaregiverDTO;
import tema1.demo.dto.CaregiverViewDTO;
import tema1.demo.entities.Caregiver;
import tema1.demo.entities.Patient;
import tema1.demo.services.CaregiverService;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    @Autowired
    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/{id}")
    public CaregiverDTO findById(@PathVariable("id") Integer id){
        return caregiverService.findUserById(id);
    }

    @GetMapping(value = "/")
    public List<CaregiverDTO> findAll(){
        return caregiverService.findAll();
    }

    @PostMapping()
    public Integer insertUserDTO(@RequestBody CaregiverDTO caregiverDTO){
        caregiverDTO.setRoleType("CAREGIVER");
        return caregiverService.insert(caregiverDTO);
    }

    @PutMapping(value = "/{id}")
    public Integer updateUser(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.update(caregiverDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@RequestBody CaregiverViewDTO caregiverViewDTO){
        caregiverService.delete(caregiverViewDTO);
    }

    @GetMapping(value = "/{id}/patients")
    public Set<Patient> getPatients(@RequestBody CaregiverDTO caregiverDTO){
        return caregiverService.getPatients(caregiverDTO);
    }

    @PostMapping(value = "/{patientId}/{caregiverId}")
    public Caregiver addList(@PathVariable("patientId") Integer patientId, @PathVariable("caregiverId") Integer caregiverId){

        return caregiverService.addList(patientId,caregiverId);

    }

}
