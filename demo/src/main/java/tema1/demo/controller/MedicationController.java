package tema1.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import tema1.demo.dto.MedicationDTO;
import tema1.demo.services.MedicationService;


import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Integer id){
        return medicationService.findUserById(id);
    }

    @GetMapping(value = "/")
    public List<MedicationDTO> findAll(){
        return medicationService.findAll();
    }

    @PostMapping(value = "/")
    public Integer insertUserDTO(@RequestBody MedicationDTO medicationDTO){
        System.out.println(medicationDTO);
        return medicationService.insert(medicationDTO);
    }

    @PutMapping(value = "/{id}")
    public Integer updateUser(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.update(medicationDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@RequestBody MedicationDTO medicationDTO){
        medicationService.delete(medicationDTO);
    }
}

