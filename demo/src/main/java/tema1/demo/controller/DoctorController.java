package tema1.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tema1.demo.dto.DoctorDTO;
import tema1.demo.dto.DoctorViewDTO;
import tema1.demo.services.DoctorService;


import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/{id}")
    public DoctorViewDTO findById(@PathVariable("id") Integer id){
        return doctorService.findUserById(id);
    }

    @GetMapping(value = "/")
    public List<DoctorViewDTO> findAll(){
        return doctorService.findAll();
    }

    @PostMapping(value = "/")
    public Integer insertUserDTO(@RequestBody DoctorDTO doctorDTO){

        doctorDTO.setRoleType("DOCTOR");
        return doctorService.insert(doctorDTO);
    }

    @PutMapping(value = "/{id}")
    public Integer updateUser(@RequestBody DoctorDTO doctorDTO) {
        return doctorService.update(doctorDTO);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@RequestBody DoctorViewDTO doctorViewDTO){
        doctorService.delete(doctorViewDTO);
    }




}
