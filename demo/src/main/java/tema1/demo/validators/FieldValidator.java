package tema1.demo.validators;

public interface FieldValidator<T> {
    boolean validate(T t);
}