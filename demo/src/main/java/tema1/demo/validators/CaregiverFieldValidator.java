package tema1.demo.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import tema1.demo.dto.CaregiverDTO;
import tema1.demo.errorhandler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class CaregiverFieldValidator {


    private static final Log LOGGER = LogFactory.getLog(CaregiverFieldValidator.class);

    public static void validateInsertOrUpdate(CaregiverDTO caregiverDTO) {

        List<String> errors = new ArrayList<>();
        if (caregiverDTO == null) {
            errors.add("caregiverDTO is null");
            throw new IncorrectParameterException(CaregiverDTO.class.getSimpleName(), errors);
        }
        if (caregiverDTO.getUsername() == null ) {
            errors.add("Person email has invalid format");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(CaregiverFieldValidator.class.getSimpleName(), errors);
        }
    }

}
