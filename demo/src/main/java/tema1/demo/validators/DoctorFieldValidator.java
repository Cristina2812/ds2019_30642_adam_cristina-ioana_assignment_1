package tema1.demo.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import tema1.demo.dto.DoctorDTO;
import tema1.demo.errorhandler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class DoctorFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(CaregiverFieldValidator.class);


    public static void validateInsertOrUpdate(DoctorDTO doctorDTO) {

        List<String> errors = new ArrayList<>();
        if (doctorDTO == null) {
            errors.add("doctorDTO is null");
            throw new IncorrectParameterException(DoctorDTO.class.getSimpleName(), errors);
        }
        if (doctorDTO.getUsername() == null ) {
            errors.add("Person email has invalid format");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DoctorFieldValidator.class.getSimpleName(), errors);
        }
    }

}
