package tema1.demo.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import tema1.demo.dto.PatientDTO;
import tema1.demo.entities.Patient;
import tema1.demo.errorhandler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;

public class PatientFieldValidator {

    private static final Log LOGGER = LogFactory.getLog(CaregiverFieldValidator.class);

    public static void validateInsertOrUpdate(PatientDTO patientDTO) {

        List<String> errors = new ArrayList<>();
        if (patientDTO == null) {
            errors.add("patientDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }
        if (patientDTO.getUsername() == null ) {
            errors.add("Person email has invalid format");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(CaregiverFieldValidator.class.getSimpleName(), errors);
        }
    }
}
