import React from 'react';
import validate from "./validators/doctor-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import CaregiverForm from '../../caregiver-data/caregiver/caregiver-form';
import Caregivers from '../../caregiver-data/caregiver/caregivers';
import { getCaregivers } from '../../caregiver-data/caregiver/api/caregiver-api';
import { Link } from 'react-router-dom';
import { browserHistory } from 'react-router';

function caregiverRedirect(){
    browserHistory.push("/Caregivers");
}


function patientRedirect(){
    browserHistory.push("/Patients");
}

function medicationPlanRedirect(){
    browserHistory.push("/MedicationPlans");
}


function medicationRedirect(){
    browserHistory.push("/Medications");
}

class DoctorForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

           formControls : {

            
            }
           


        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);



        
    }


    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;



        const updatedControls = {
            ...this.state.formControls
        };

  
   
        const updatedFormElement = {
            ...updatedControls[name]
        };
        
 

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        
        


        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);
        
       
        updatedControls[name] = updatedFormElement;
        
        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

   
      
        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

        
   
    };





    handleSubmit(){
        

    }
   
    
    render() {
        return (

          <form onSubmit={this.handleSubmit}>
           
              <p></p>
              
                <Button onClick={caregiverRedirect} 
                        variant = "success"
                        type = {"submit"}
                >Add Caregiver</Button>

                <p></p>
                <Button onClick={patientRedirect} 
                        variant = "success"
                        type = {"submit"}
                >Add Patient</Button>

                <p></p>
                <Button onClick={medicationRedirect} 
                        variant = "success"
                        type = {"submit"}
                >Add Medication</Button>
              

              <p></p>
                <Button onClick={medicationPlanRedirect} 
                        variant = "success"
                        type = {"submit"}
                >Add MedicationPlan</Button>
            

            </form>
        
        );
    }
}

export default DoctorForm;
