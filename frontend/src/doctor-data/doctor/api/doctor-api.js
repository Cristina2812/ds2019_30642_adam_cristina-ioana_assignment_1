import { HOST } from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_doctors: '/doctor/',
    post_doctor: "/doctor/",
    get_patients: '/patient/',
    post_patient: "/patient/",
    post_login: "/login"
};

function getDoctors(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_doctors, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDoctorById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_doctors + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postDoctor(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_doctor, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_patient, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiverById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postCaregiver(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_patient, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function postLogin(user, callback) {
    
    // console.log(token) // logs token
    console.log(user)

    let request = new Request(HOST.backend_api + endpoint.post_login, {
        method: 'POST',
        
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    });
  
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getDoctors,
    getDoctorById,
    postDoctor,
    getPatients,
    getPatientById,
    postPatient,
    getCaregivers,
    getCaregiverById,
    postCaregiver,
    postLogin
};
