export default function* apiCall(path, options, actionType){

    try {
      const response = yield call(fetch, `http://blah/${path}`, options);
      const data = yield call([response, response.json]);
  
      // call reducer
      yield put({type: actionType, payload: data});
    } catch (e) {
      console.log(e.message);
      console.log(`error api call for ${actionType}`);
    }
  }