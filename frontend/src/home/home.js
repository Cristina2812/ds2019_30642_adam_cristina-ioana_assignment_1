import React, { useState } from "react";
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import "./Login.css";
import BackgroundImg from '../commons/images/future-medicine.jpg';
import { postLogin } from '../doctor-data/doctor/api/doctor-api';
import TextInput from "../doctor-data/doctor/fields/TextInput";
import validate from "./validators/caregiver-validators";


const backgroundStyle = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  width: "100%",
  height: "1920px",
  backgroundImage: `url(${BackgroundImg})`
};
const textStyle = { color: 'white' };


export default class Login extends React.Component {

  constructor(props) {
    super(props);

    this.state = {

      errorStatus: 0,
      error: null,

      formIsValid: false,
      formControls: {
        username: {
          value: '',
          placeholder: 'Unique username',
          valid: false,
          touched: false,

        },
        password: {
          value: '',
          placeholder: 'Password',
          valid: false,
          touched: false,

        }

      }
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    // this.setUsername = this.setUsername.bind(this);
    // this.setPassword = this.setPassword.bind(this);
  }

  // validateForm() {
  //   return this.state.username.length > 0 && this.state.password.length > 0;
  // }

  // setUsername(event) {
  //   const { username } = this.state;
  //   username = event.target.value;
  //   this.setState({
  //     ...this.state,
  //     username
  //   })
  // }

  // setPassword(event) {
  //   const { password } = this.state;
  //   password = event.target.value;
  //   this.setState({
  //     ...this.state,
  //     password
  //   })
  // }

  handleChange = event => {

    const name = event.target.name;
    const value = event.target.value;



    const updatedControls = {
      ...this.state.formControls
    };



    const updatedFormElement = {
      ...updatedControls[name]
    };



    updatedFormElement.value = value;
    updatedFormElement.touched = true;
     updatedFormElement.valid = validate(value, updatedFormElement.validationRules);





     console.log("Element: " + name + " validated: " + updatedFormElement.valid);


    updatedControls[name] = updatedFormElement;

    let formIsValid = true;
    for (let updatedFormElementName in updatedControls) {
      formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
    }



    this.setState({
      formControls: updatedControls,
      formIsValid: formIsValid
    });



  };

  loginUser(user){
    return postLogin(user, (result, status, error) => {
        // console.log("postlogin", result);

        // if(result !== null && (status === 200 || status ===201)){
        //     console.log("Successfully inserted caregiver with id: " + result);
            
        //     this.props.refresh();
        // } else {
        //     this.state.errorStatus = status;
        //     this.error = error;
        // }
    });
}

  handleSubmit(event) {

    let user = {
      username: this.state.formControls.username.value,
      password: this.state.formControls.password.value,
    };

    this.loginUser(user);
    // // fetch('/login', {
    // //   method: 'POST',
    // //   headers: {
    // //       'Accept': 'application/json',
    // //       'Content-Type': 'application/json'
    // //   },
    // //   body: JSON.stringify(user)
    // // })
    // //   .then(response => response)
    // //   .then(data => console.log(data.headers))
    // //   .catch(err => console.log(err));

    // return postLogin({ username: this.state.username, password: this.state.password }, (result, status, error) => {
    //   console.log(result);



    //   // var token = localStorage.getItem('my_tkn');

    //   if (result !== null && (status === 200 || status === 201)) {
    //     console.log("Successfully logged " + result);
    //     this.props.refresh();
    //   } else {
    //     this.state.errorStatus = status;
    //     this.error = error;
    //   }
    // });
  }

  render() {
    return (
      <div className="Login">
        {/* <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="username" bsSize="large">
            <FormLabel>Username</FormLabel>
            <FormControl
              autoFocus
              type="username"
              value={this.state.username}
              onChange={e => this.setUsername(e.target.value)}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <FormLabel>Password</FormLabel>
            <input type='text'
              value={this.state.password}
              onChange={e => this.setPassword(e.target.value)}
              type="password"
            />
          </FormGroup>
          <Button block bsSize="large" disabled={!this.validateForm()} type="submit">
            Login
          </Button>
        </form> */}
        <form onSubmit={this.handleSubmit}>


          <p> Username: </p>
          <TextInput name="username"
            placeholder={this.state.formControls.username.placeholder}
            value={this.state.formControls.username.value}
            onChange={this.handleChange}
            touched={this.state.formControls.username.touched}
            // valid={this.state.formControls.username.valid}
          />

          <p> Password: </p>
          <TextInput name="password"
            placeholder={this.state.formControls.password.placeholder}
            value={this.state.formControls.password.value}
            onChange={this.handleChange}
            touched={this.state.formControls.password.touched}
            // valid={this.state.formControls.password.valid}
          />

          <p></p>
          <Button variant="success"
            onClick={this.handleSubmit}>
            Login
          </Button>

        </form>
      </div>
    );
  }
}