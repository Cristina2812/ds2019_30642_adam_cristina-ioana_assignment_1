import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";


const endpoint = {
    get_medicatioPlans: '/medicatioPlan/',
    post_medicatioPlan: "/medicatioPlan/",
    get_medications: '/medication/',
    post_medication: "/medication/",
    get_patients: '/patient/',
    post_patient: "/patient/",
    addMedPlan: "/addMedPlan",
};

function getMedicatioPlans(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medicatioPlans, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicatioPlans(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medicatioPlans, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function getMedicatioPlanById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_medicatioPlans + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedicatioPlan(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_medicatioPlan , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medications, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_medications + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedication(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_medication , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_patients + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.post_patient , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function addMedPlan(getPatientById, getMedicationById,callback){
    let request = new Request(HOST.backend_api + endpoint.get_patients + params.id+endpoint.get_medications+params.id, {
        method: 'GET'
     });
 
     console.log(request.url);
     RestApiClient.performRequest(request, callback);
}


export {
    getMedicatioPlans,
    getMedicatioPlanById,
    postMedicatioPlan,
    getMedications,
    getMedicationById,
    postMedication,
    getPatients,
    getPatientById,
    postPatient,
    addMedPlan

};
