import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import MedicatioPlanForm from "./medicatioPlan-form";

import * as API_USERS from "./api/medicatioPlan-api"


const columns = [
    
   
    {
        Header:  'IntakeInterval',
        accessor: 'intakeInterval',
    },
    {
        Header: 'Period',
        accessor: 'period',
    },



];

const filters = [
  
    {
        accessor: 'intakeInterval',
    },
    {
        accessor: 'IntakeInterval',
    },
  
  
   
];

class MedicatioPlans extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchMedicatioPlans();
    }

    fetchMedicatioPlans() {
        return API_USERS.getMedicatioPlans((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({   
                    intakeInterval: x.intakeInterval,
                    IntakeInterval: x.IntakeInterval,
                       
                   });
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.forceUpdate();
           }
        });
    }

    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({
                       name: x.name,
                       email: x.username,
                   });
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.state.errorStatus = status;
               this.state.error = err;
               this.forceUpdate();
           }
        });
    }

    fetchMedications() {
        return API_USERS.getMedications((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({   
                    name: x.name,
                    dosage: x.dosage,
                    effect: x.effect,
                       
                   });
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.forceUpdate();
           }
        });
    }


    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <MedicatioPlanForm registerMedicatioPlan={this.refresh}>

                                </MedicatioPlanForm>

                                <MedicationForm registerMedication={this.refresh}>

                                </MedicationForm>

                                <PatientForm registerPatient={this.refresh}>

                                </PatientForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default MedicatioPlans;
