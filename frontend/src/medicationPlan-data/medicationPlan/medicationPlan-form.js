import React from 'react';
import TextInput from "./fields/TextInput";
import validate from "./validator/medication-validators";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class MedicationForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

           formControls : {

            intakeInterval: {
                value: '',
                placeholder: 'Insert intakeInterval',
                valid: false,
                touched: false,
                validationRules: {
                    minLength: 3,
                    isRequired: true
                }
            },

            period: {
                value: '',
                placeholder: 'Period',
                valid: false,
                touched: false,
              
            },
        

           }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const intakeInterval = event.target.intakeInterval;
        const value = event.target.value;



        const updatedControls = {
            ...this.state.formControls
        };

  
   
        const updatedFormElement = {
            ...updatedControls[intakeInterval]
        };
        
 

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        
        


        console.log("Element: " +  intakeInterval + " validated: " + updatedFormElement.valid);
        
       
        updatedControls[intakeInterval] = updatedFormElement;
        
        let formIsValid = true;
        for (let updatedFormElementIntakeInterval in updatedControls) {
            formIsValid = updatedControls[updatedFormElementIntakeInterval].valid && formIsValid;
        }

   
      
        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

        
   
    };

    registerMedication(medication){
        return API_USERS.postMedication(medication, (result, status, error) => {
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted medication with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }



    handleSubmit(){

        console.log("New medication:");
        console.log("IntakeInterval: " + this.state.formControls.intakeInterval.value);
        console.log("Period: " + this.state.formControls.period.value);
       
        let user = {
            intakeInterval: this.state.formControls.intakeInterval.value,
            period : this.state.formControls.period.value,
           
        };


        this.registerMedication(user);

    }

    render() {
        return (

            <form onSubmit={this.handleSubmit}>
  
                <h1>Insert new medication</h1>
  
                <p> IntakeInterval: </p>
  
                <TextInput intakeInterval="intakeInterval"
                           placeholder={this.state.formControls.intakeInterval.placeholder}
                           value={this.state.formControls.intakeInterval.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.intakeInterval.touched}
                           valid={this.state.formControls.intakeInterval.valid}
                />
                {this.state.formControls.intakeInterval.touched && !this.state.formControls.intakeInterval.valid &&
                <div classIntakeInterval={"error-message row"}> * IntakeInterval must have at least 3 characters </div>}
  
                <p> Period: </p>
                <TextInput intakeInterval="period"
                           placeholder={this.state.formControls.period.placeholder}
                           value={this.state.formControls.period.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.period.touched}
                           valid={this.state.formControls.period.valid}
                />
              
  
                <p></p>
                <Button variant="success"
                        type={"submit"}
                        disabled={!this.state.formIsValid}>
                    Add Medication
                </Button>
  
  
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                 
  
               
            </form>
          
          );
      }
}

export default MedicationForm;
