import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home'
import Caregivers from './caregiver-data/caregiver/caregivers'
import Doctors from './doctor-data/doctor/doctors'
import Patients from './patient-data/patient/patients'
import Medications from './medication-data/medication/medications'


import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
//import Login from "./containers/Login";


let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />
                        {/* <Route path="/login" 
                                exact 
                                component={Login} /> */}

                        <Route
                            exact
                            path='/caregivers'
                            render={() => <Caregivers/>}
                        />

                        <Route
                            exact
                            path='/doctors'
                            render={() => <Doctors/>}
                        />

                        <Route
                            exact
                            path='/patients'
                            render={() => <Patients/>}
                        />

                        <Route
                            exact
                            path='/medications'
                            render={() => <Medications/>}
                        />


                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />
                        

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
