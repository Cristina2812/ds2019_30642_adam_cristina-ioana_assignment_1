import React from 'react';
import TextInput from "./fields/TextInput";
import validate from "./validator/medication-validators";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class MedicationForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

           formControls : {

            name: {
                value: '',
                placeholder: 'Insert name',
                valid: false,
                touched: false,
                validationRules: {
                    minLength: 3,
                    isRequired: true
                }
            },

            effect: {
                value: '',
                placeholder: 'Effect',
                valid: false,
                touched: false,
              
            },

            dosage: {
                value: '',
                placeholder: 'Dosage...',
                valid: false,
                touched: false,

            },
        

           }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;



        const updatedControls = {
            ...this.state.formControls
        };

  
   
        const updatedFormElement = {
            ...updatedControls[name]
        };
        
 

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        
        


        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);
        
       
        updatedControls[name] = updatedFormElement;
        
        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

   
      
        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

        
   
    };

    registerMedication(medication){
        return API_USERS.postMedication(medication, (result, status, error) => {
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted medication with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }



    handleSubmit(){

        console.log("New medication:");
        console.log("Name: " + this.state.formControls.name.value);
        console.log("Effect: " + this.state.formControls.effect.value);
        console.log("Dosage: " + this.state.formControls.dosage.value);
       
        let user = {
            name: this.state.formControls.name.value,
            effect : this.state.formControls.effect.value,
            dosage: this.state.formControls.dosage.value,
           
        };


        this.registerMedication(user);

    }

    render() {
        return (

            <form onSubmit={this.handleSubmit}>
  
                <h1>Insert new medication</h1>
  
                <p> Name: </p>
  
                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />
                {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                <div className={"error-message row"}> * Name must have at least 3 characters </div>}
  
                <p> Effect: </p>
                <TextInput name="effect"
                           placeholder={this.state.formControls.effect.placeholder}
                           value={this.state.formControls.effect.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.effect.touched}
                           valid={this.state.formControls.effect.valid}
                />
              
  
  
                <p> Dosage: </p>
                <TextInput name="dosage"
                           placeholder={this.state.formControls.dosage.placeholder}
                           value={this.state.formControls.dosage.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.dosage.touched}
                           valid={this.state.formControls.dosage.valid}
                />
  
                <p></p>
                <Button variant="success"
                        type={"submit"}
                        disabled={!this.state.formIsValid}>
                    Add Medication
                </Button>
  
  
                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}
                 
  
               
            </form>
          
          );
      }
}

export default MedicationForm;
